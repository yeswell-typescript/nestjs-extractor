import { inspect } from 'util';

import { InjectionToken } from '@nestjs/common/interfaces/modules/injection-token.interface';

import { BaseException } from './base-exception';

class ProviderNotInstancedException extends BaseException {
	public constructor(token: InjectionToken) {
		super(`Provider for token '${inspect(token)}' not instanced`);
	}
}

export { ProviderNotInstancedException };
