import { inspect } from 'util';

import { Type } from '@nestjs/common';

import { BaseException } from './base-exception';

class ModuleNotImportedException extends BaseException {
	public constructor(extractorModuleClass: Type) {
		super(`Extractor module '${inspect(extractorModuleClass)}' not imported`);
	}
}

export { ModuleNotImportedException };
