import { inspect } from 'util';

import { Type } from '@nestjs/common';

import { BaseException } from './base-exception';

class MultipleImportException extends BaseException {
	public constructor(extractorModuleClass: Type) {
		super(`Extractor module '${inspect(extractorModuleClass)}' imported to several module`);
	}
}

export { MultipleImportException };
