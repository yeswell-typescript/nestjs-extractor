import { inspect } from 'util';

import { Type } from '@nestjs/common';

import { BaseException } from './base-exception';

class ModuleNotFoundException extends BaseException {
	public constructor(extractorModuleClass: Type) {
		super(`Extractor module '${inspect(extractorModuleClass)}' not found`);
	}
}

export { ModuleNotFoundException };
