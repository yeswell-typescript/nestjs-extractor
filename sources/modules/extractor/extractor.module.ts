import { DynamicModule } from '@nestjs/common';
import { InjectionToken } from '@nestjs/common/interfaces/modules/injection-token.interface';

import { buildExtractionProvider, buildExtractorModuleOptions } from './functions';
import { ExtractorModuleOptions } from './types';

class ExtractorModule {
	public static forFeature(tokens: InjectionToken[], options: Partial <ExtractorModuleOptions> = {}): DynamicModule {
		const builtOptions = buildExtractorModuleOptions(options);

		class CurrentExtractorModule extends ExtractorModule {}

		const providers = tokens.map((token) => buildExtractionProvider(token, CurrentExtractorModule, builtOptions));

		return {
			module: CurrentExtractorModule,

			providers,

			exports: providers.map(({ provide }) => provide),
		};
	}
}

export { ExtractorModule };
