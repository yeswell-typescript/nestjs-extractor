type ExtractorModuleOptions = {
	optional: boolean;
};

export { ExtractorModuleOptions };
