import { ExtractorModuleOptions } from '../types';

const defaultExtractorModuleOptions: ExtractorModuleOptions = {
	optional: false,
};

function buildExtractorModuleOptions(options: Partial <ExtractorModuleOptions>): ExtractorModuleOptions {
	return {
		...defaultExtractorModuleOptions,
		...options,
	};
}

export { buildExtractorModuleOptions };
