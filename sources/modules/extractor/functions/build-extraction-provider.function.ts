import { ModuleRef, NestContainer } from '@nestjs/core';
import { Injector } from '@nestjs/core/injector/injector';

import { Type } from '@nestjs/common';
import { InjectionToken } from '@nestjs/common/interfaces/modules/injection-token.interface';
import { FactoryProvider } from '@nestjs/common/interfaces/modules/provider.interface';

import {
	ModuleNotFoundException,
	ModuleNotImportedException,
	MultipleImportException,
	ProviderNotFoundException,
	ProviderNotInstancedException,
} from '../exceptions';

import { ExtractorModuleOptions } from '../types';

import { findProviderModule } from './find-provider-module.function';

type HackedModuleRef = ModuleRef & {
	container: NestContainer;
};

const injector = new Injector();

function buildExtractionProvider(token: InjectionToken, extractorModuleClass: Type, options: ExtractorModuleOptions): FactoryProvider {
	return {
		provide: token,

		inject: [
			ModuleRef,
		],

		async useFactory(moduleRef: HackedModuleRef): Promise <unknown> {
			const modulesMap = moduleRef.container.getModules();
			const modules = [...modulesMap.values()];

			const currentExtractorModule = modules.find((module) => (module.metatype === extractorModuleClass));

			if (!currentExtractorModule) {
				throw new ModuleNotFoundException(extractorModuleClass);
			}

			const parentModules = modules.filter(module => module.imports.has(currentExtractorModule));

			if (parentModules.length > 1) {
				throw new MultipleImportException(extractorModuleClass);
			}

			const [parentModule = null] = parentModules;

			if (parentModule === null) {
				throw new ModuleNotImportedException(extractorModuleClass);
			}

			const providerModule = findProviderModule(token, parentModule, currentExtractorModule, modules);

			if (providerModule === null) {
				if (options.optional) {
					return;
				}

				throw new ProviderNotFoundException(token);
			}

			const instanceWrapper = providerModule.getProviderByKey(token);

			await injector.loadProvider(instanceWrapper, providerModule);

			const { instance } = instanceWrapper;

			if (instance === null) {
				throw new ProviderNotInstancedException(token);
			}

			return instance;
		},
	};
}

export { buildExtractionProvider };
