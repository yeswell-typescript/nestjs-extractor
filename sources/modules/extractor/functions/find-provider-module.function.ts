import { Module } from '@nestjs/core/injector/module';

import { InjectionToken } from '@nestjs/common/interfaces/modules/injection-token.interface';

function findProviderModule(token: InjectionToken, parentModule: Module, extractorModule: Module, modules: Module[]): Module | null {
	const checkedModules = new Set <Module> ();

	let parentModules = [parentModule];

	while (parentModules.length > 0) {
		const nextParentModules: Module[] = [];

		for (const currentModule of parentModules) {
			if (currentModule.providers.has(token)) {
				return currentModule;
			}

			const importedModules = [...currentModule.imports.values()];

			const importedModule = importedModules.find(
				(importedModule) => ((importedModule !== extractorModule) && importedModule.exports.has(token)),
			);

			if (importedModule) {
				return importedModule;
			}

			checkedModules.add(currentModule);

			nextParentModules.push(
				...modules.filter(module => (!checkedModules.has(module) && module.imports.has(currentModule))),
			);
		}

		parentModules = nextParentModules;
	}

	return null;
}

export { findProviderModule };
