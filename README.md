# Description

[![npm version](https://badge.fury.io/js/nestjs-extractor.svg)](https://badge.fury.io/js/nestjs-extractor)
[![install size](https://packagephobia.now.sh/badge?p=nestjs-extractor)](https://packagephobia.now.sh/result?p=nestjs-extractor)

NestJS module for extract providers from parent non-global modules

## Installation

```bash
npm install --save nestjs-extractor
```

## Usage

Create **ZooConfigService** and **ZooConfigModule**

```ts
@Injectable()
class ZooConfigService {
    public get zooName(): string {
        return process.env.ZOO_NAME ?? 'Central Park Zoo';
    }
}

@Module({
    providers: [ZooConfigService],
    exports: [ZooConfigService],
})
class ZooConfigModule {}
```

Provide **ZooConfigService** through **ExtractorModule** in **CatModule**...

```ts
import { ExtractorModule } from 'nestjs-extractor';

@Module({
    imports: [ExtractorModule.forFeature([ZooConfigService])],
    providers: [CatService],
    controllers: [CatController],
})
class CatModule {}
```

...and use **ZooConfigService** in **CatController**...

```ts
@Controller()
class CatController {
    public constructor(
        private readonly catService: CatService,
        private readonly zooConfigService: ZooConfigService,
    ) {}

    @Get('/cat/:catId/info')
    public getCatInfo(
        @Param('catId', ParseIntPipe) catId: number,
    ): string {
        const catName = this.catService.getCatById(catId);
        const zooName = this.zooConfigService.zooName;

        return `Cat ${catName} from ${zooName}`;
    }
}
```

Import **CatModule** in **ZooModule**

```ts
@Module({
    imports: [CatModule, DogModule],
})
class ZooModule {}
```

Import **ZooConfigModule** and **ZooModule** in **AppModule**

```ts
@Module({
    imports: [ZooConfigModule, ZooModule],
})
class AppModule {}
```

## Options

You can provide the options for the providers search as the second argument of the `ExtractorModule.forFeature()` function

```ts
type ExtractorModuleOptions = {
    optional?: boolean;
};
```

1. `optional` is needed for providers marked as `@Optional()`

```ts
@Injectable()
class Service {
    public constructor(
        @Optional()
        public readonly nestedService: NestedService,
    ) {}
}

@Module({
    imports: [
        ExtractorModule.forFeature([NestedService], { optional: true }),
    ],

    providers: [Service],
})
class ServiceModule {}
```

## License

MIT
