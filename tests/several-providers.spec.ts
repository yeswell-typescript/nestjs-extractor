import { INestApplication, Injectable, Module } from '@nestjs/common';

import { expect } from 'expect';
import { afterEach } from 'mocha';

import { ExtractorModule, ExtractorModuleException } from '../sources';

import { createTestApplication } from './create-test-application.function';

describe('ExtractorModule: several providers', () => {
	let application: INestApplication | null = null;

	afterEach(async () => {
		if (application !== null) {
			await application.close();

			application = null;
		}
	});

	class FirstNestedService {
		public constructor(
			public readonly firstValue: string,
		) {
		}
	}

	class SecondNestedService {
		public constructor(
			public readonly secondValue: string,
		) {
		}
	}

	@Injectable()
	class Service {
		public constructor(
			public readonly firstNestedService: FirstNestedService,
			public readonly secondNestedService: SecondNestedService,
		) {
		}
	}

	@Module({
		imports: [
			ExtractorModule.forFeature([FirstNestedService, SecondNestedService]),
		],

		providers: [
			Service,
		],
	})
	class ServiceModule {}

	it('should throw ProviderNotFoundException if least one provider not found', async () => {
		const firstNestedServiceValue = 'first-nested-service-value';

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				{
					provide: FirstNestedService,
					useValue: {
						value: firstNestedServiceValue,
					},
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		const applicationPromise = createTestApplication(ApplicationModule);

		await expect(applicationPromise).rejects.toThrow(ExtractorModuleException.ProviderNotFoundException);
	});

	it('should extract providers from one module', async () => {
		const firstNestedServiceValue = 'first-nested-service-value';
		const secondNestedServiceValue = 'second-nested-service-value';

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				{
					provide: FirstNestedService,
					useValue: {
						value: firstNestedServiceValue,
					},
				},

				{
					provide: SecondNestedService,
					useValue: {
						value: secondNestedServiceValue,
					},
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.firstNestedService).toEqual({
			value: firstNestedServiceValue,
		});

		expect(service.secondNestedService).toEqual({
			value: secondNestedServiceValue,
		});
	});

	it('should extract providers from different modules', async () => {
		const firstNestedServiceValue = 'first-nested-service-value';
		const secondNestedServiceValue = 'second-nested-service-value';

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				{
					provide: FirstNestedService,
					useValue: {
						value: firstNestedServiceValue,
					},
				},
			],
		})
		class FirstParentModule {}

		@Module({
			imports: [
				FirstParentModule,
			],

			providers: [
				{
					provide: SecondNestedService,
					useValue: {
						value: secondNestedServiceValue,
					},
				},
			],
		})
		class SecondParentModule {}

		@Module({
			imports: [
				SecondParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.firstNestedService).toEqual({
			value: firstNestedServiceValue,
		});

		expect(service.secondNestedService).toEqual({
			value: secondNestedServiceValue,
		});
	});
});
