import { INestApplication, Injectable, Module } from '@nestjs/common';

import { expect } from 'expect';
import { afterEach } from 'mocha';

import { ExtractorModule, ExtractorModuleException } from '../sources';

import { createTestApplication } from './create-test-application.function';

describe('ExtractorModule: parent imports', () => {
	let application: INestApplication | null = null;

	afterEach(async () => {
		if (application !== null) {
			await application.close();

			application = null;
		}
	});

	class NestedService {
		public constructor(
			public readonly value: string,
		) {
		}
	}

	@Injectable()
	class Service {
		public constructor(
			public readonly nestedService: NestedService,
		) {
		}
	}

	@Module({
		imports: [
			ExtractorModule.forFeature([NestedService]),
		],

		providers: [
			Service,
		],
	})
	class ServiceModule {}

	it('should throw ProviderNotFoundException if provider not found in parent import', async () => {
		const nestedServiceValue = 'nested-service-value';

		@Module({
			providers: [
				{
					provide: NestedService,
					useValue: {
						value: nestedServiceValue,
					},
				},
			],
		})
		class SomeModule {}

		@Module({
			imports: [
				ServiceModule,
				SomeModule,
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		const applicationPromise = createTestApplication(ApplicationModule);

		await expect(applicationPromise).rejects.toThrow(ExtractorModuleException.ProviderNotFoundException);
	});

	it('should extract from parent import with useValue provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		@Module({
			providers: [
				{
					provide: NestedService,
					useValue: {
						value: nestedServiceValue,
					},
				},
			],

			exports: [
				NestedService,
			],
		})
		class SomeModule {}

		@Module({
			imports: [
				ServiceModule,
				SomeModule,
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from parent import with useClass provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		class MyNestedService extends NestedService {
			public constructor() {
				super(nestedServiceValue);
			}
		}

		@Module({
			providers: [
				{
					provide: NestedService,
					useClass: MyNestedService,
				},
			],

			exports: [
				NestedService,
			],
		})
		class SomeModule {}

		@Module({
			imports: [
				ServiceModule,
				SomeModule,
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from parent import with useFactory provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		class SomeService {}

		@Module({
			providers: [
				SomeService,

				{
					provide: NestedService,
					inject: [SomeService],
					useFactory(): NestedService {
						return {
							value: nestedServiceValue,
						};
					},
				},
			],

			exports: [
				NestedService,
			],
		})
		class SomeModule {}

		@Module({
			imports: [
				ServiceModule,
				SomeModule,
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from parent import with useExisting provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		class MyNestedService extends NestedService {
			public constructor() {
				super(nestedServiceValue);
			}
		}

		@Module({
			providers: [
				MyNestedService,

				{
					provide: NestedService,
					useExisting: MyNestedService,
				},
			],

			exports: [
				NestedService,
			],
		})
		class SomeModule {}

		@Module({
			imports: [
				ServiceModule,
				SomeModule,
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});
});
