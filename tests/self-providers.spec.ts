import { INestApplication, Injectable, Module } from '@nestjs/common';

import { expect } from 'expect';
import { afterEach } from 'mocha';

import { ExtractorModule, ExtractorModuleException } from '../sources';

import { createTestApplication } from './create-test-application.function';

describe('ExtractorModule: self providers', () => {
	let application: INestApplication | null = null;

	afterEach(async () => {
		if (application !== null) {
			await application.close();

			application = null;
		}
	});

	class NestedService {
		public constructor(
			public readonly value: string,
		) {
		}
	}

	@Injectable()
	class Service {
		public constructor(
			public readonly nestedService: NestedService,
		) {
		}
	}

	it('should throw ProviderNotFoundException if self provider not found', async () => {
		@Module({
			imports: [
				ExtractorModule.forFeature([NestedService]),
			],

			providers: [
				Service,
			],
		})
		class ServiceModule {}

		@Module({
			imports: [
				ServiceModule,
			],
		})
		class ApplicationModule {}

		const applicationPromise = createTestApplication(ApplicationModule);

		await expect(applicationPromise).rejects.toThrow(ExtractorModuleException.ProviderNotFoundException);
	});

	it('should extract from self useValue provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		@Module({
			imports: [
				ExtractorModule.forFeature([NestedService]),
			],

			providers: [
				{
					provide: NestedService,
					useValue: {
						value: nestedServiceValue,
					},
				},

				Service,
			],
		})
		class ServiceModule {}

		@Module({
			imports: [
				ServiceModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from self useClass provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		class MyNestedService extends NestedService {
			public constructor() {
				super(nestedServiceValue);
			}
		}

		@Module({
			imports: [
				ExtractorModule.forFeature([NestedService]),
			],

			providers: [
				{
					provide: NestedService,
					useClass: MyNestedService,
				},

				Service,
			],
		})
		class ServiceModule {}

		@Module({
			imports: [
				ServiceModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from self useFactory provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		class SomeService {}

		@Module({
			imports: [
				ExtractorModule.forFeature([NestedService]),
			],

			providers: [
				SomeService,

				{
					provide: NestedService,
					inject: [SomeService],
					useFactory(): NestedService {
						return {
							value: nestedServiceValue,
						};
					},
				},

				Service,
			],
		})
		class ServiceModule {}

		@Module({
			imports: [
				ServiceModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from self useExisting provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		class MyNestedService extends NestedService {
			public constructor() {
				super(nestedServiceValue);
			}
		}

		@Module({
			imports: [
				ExtractorModule.forFeature([NestedService]),
			],

			providers: [
				MyNestedService,

				{
					provide: NestedService,
					useExisting: MyNestedService,
				},

				Service,
			],
		})
		class ServiceModule {}

		@Module({
			imports: [
				ServiceModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});
});
