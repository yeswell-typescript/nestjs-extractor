import { INestApplication, Injectable, Module } from '@nestjs/common';

import { expect } from 'expect';
import { afterEach } from 'mocha';

import { ExtractorModule, ExtractorModuleException } from '../sources';

import { createTestApplication } from './create-test-application.function';

describe('ExtractorModule: parent providers', () => {
	let application: INestApplication | null = null;

	afterEach(async () => {
		if (application !== null) {
			await application.close();

			application = null;
		}
	});

	class NestedService {
		public constructor(
			public readonly value: string,
		) {
		}
	}

	@Injectable()
	class Service {
		public constructor(
			public readonly nestedService: NestedService,
		) {
		}
	}

	@Module({
		imports: [
			ExtractorModule.forFeature([NestedService]),
		],

		providers: [
			Service,
		],
	})
	class ServiceModule {}

	it('should throw ProviderNotFoundException if parent provider not found', async () => {
		@Module({
			imports: [
				ServiceModule,
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		const applicationPromise = createTestApplication(ApplicationModule);

		await expect(applicationPromise).rejects.toThrow(ExtractorModuleException.ProviderNotFoundException);
	});

	it('should extract from parent useValue provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				{
					provide: NestedService,
					useValue: {
						value: nestedServiceValue,
					},
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from parent useClass provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		class MyNestedService extends NestedService {
			public constructor() {
				super(nestedServiceValue);
			}
		}

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				{
					provide: NestedService,
					useClass: MyNestedService,
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from parent useFactory provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		class SomeService {}

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				SomeService,

				{
					provide: NestedService,
					inject: [SomeService],
					useFactory(): NestedService {
						return {
							value: nestedServiceValue,
						};
					},
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from parent useExisting provider', async () => {
		const nestedServiceValue = 'nested-service-value';

		class MyNestedService extends NestedService {
			public constructor() {
				super(nestedServiceValue);
			}
		}

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				MyNestedService,

				{
					provide: NestedService,
					useExisting: MyNestedService,
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});
});
