import { INestApplication, Type } from '@nestjs/common';
import { Test } from '@nestjs/testing';

async function createTestApplication(applicationModule: Type): Promise <INestApplication> {
	const testingModuleBuilder = Test.createTestingModule({
		imports: [applicationModule],
	});

	const testingModule = await testingModuleBuilder.compile();

	const application = testingModule.createNestApplication();

	return application.init();
}

export { createTestApplication };
