import { INestApplication, Injectable, Module, Optional } from '@nestjs/common';

import { expect } from 'expect';
import { afterEach } from 'mocha';

import { ExtractorModule } from '../sources';

import { createTestApplication } from './create-test-application.function';

describe('ExtractorModule: options optional', () => {
	let application: INestApplication | null = null;

	afterEach(async () => {
		if (application !== null) {
			await application.close();

			application = null;
		}
	});

	class NestedService {
		public constructor(
			public readonly value: string,
		) {
		}
	}

	@Injectable()
	class Service {
		public constructor(
			@Optional()
			public readonly nestedService: NestedService,
		) {
		}
	}

	@Module({
		imports: [
			ExtractorModule.forFeature([NestedService], { optional: true }),
		],

		providers: [
			Service,
		],
	})
	class ServiceModule {}

	it('should extract undefined if parent provider not found when options optional is true', async () => {
		@Module({
			imports: [
				ServiceModule,
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toBeUndefined();
	});

	it('should extract from parent useValue provider when options optional is true', async () => {
		const nestedServiceValue = 'nested-service-value';

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				{
					provide: NestedService,
					useValue: {
						value: nestedServiceValue,
					},
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from parent useClass provider when options optional is true', async () => {
		const nestedServiceValue = 'nested-service-value';

		class MyNestedService extends NestedService {
			public constructor() {
				super(nestedServiceValue);
			}
		}

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				{
					provide: NestedService,
					useClass: MyNestedService,
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from parent useFactory provider when options optional is true', async () => {
		const nestedServiceValue = 'nested-service-value';

		class SomeService {}

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				SomeService,

				{
					provide: NestedService,
					inject: [SomeService],
					useFactory(): NestedService {
						return {
							value: nestedServiceValue,
						};
					},
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});

	it('should extract from parent useExisting provider when options optional is true', async () => {
		const nestedServiceValue = 'nested-service-value';

		class MyNestedService extends NestedService {
			public constructor() {
				super(nestedServiceValue);
			}
		}

		@Module({
			imports: [
				ServiceModule,
			],

			providers: [
				MyNestedService,

				{
					provide: NestedService,
					useExisting: MyNestedService,
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const service = application.get(Service);

		expect(service.nestedService).toEqual({
			value: nestedServiceValue,
		});
	});
});
