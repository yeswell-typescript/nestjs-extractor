import { INestApplication, Injectable, Module } from '@nestjs/common';

import { expect } from 'expect';
import { afterEach } from 'mocha';

import { ExtractorModule, ExtractorModuleException } from '../sources';

import { createTestApplication } from './create-test-application.function';

describe('ExtractorModule: multiple import', () => {
	let application: INestApplication | null = null;

	afterEach(async () => {
		if (application !== null) {
			await application.close();

			application = null;
		}
	});

	class NestedService {
		public constructor(
			public readonly value: string,
		) {
		}
	}

	@Injectable()
	class FirstService {
		public constructor(
			public readonly nestedService: NestedService,
		) {
		}
	}

	@Injectable()
	class SecondService {
		public constructor(
			public readonly nestedService: NestedService,
		) {
		}
	}

	it('should throw MultipleImportException if reuse ExtractorModule.forFeature()', async () => {
		const nestedServiceValue = 'nested-service-value';
		const extractorModule = ExtractorModule.forFeature([NestedService]);

		@Module({
			imports: [
				extractorModule,
			],

			providers: [
				FirstService,
			],
		})
		class FirstServiceModule {}

		@Module({
			imports: [
				extractorModule,
			],

			providers: [
				SecondService,
			],
		})
		class SecondServiceModule {}

		@Module({
			imports: [
				FirstServiceModule,
				SecondServiceModule,
			],

			providers: [
				{
					provide: NestedService,
					useValue: {
						value: nestedServiceValue,
					},
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		const applicationPromise = createTestApplication(ApplicationModule);

		await expect(applicationPromise).rejects.toThrow(ExtractorModuleException.MultipleImportException);
	});

	it('should extract one provider for two modules', async () => {
		const nestedServiceValue = 'nested-service-value';

		@Module({
			imports: [
				ExtractorModule.forFeature([NestedService]),
			],

			providers: [
				FirstService,
			],
		})
		class FirstServiceModule {}

		@Module({
			imports: [
				ExtractorModule.forFeature([NestedService]),
			],

			providers: [
				SecondService,
			],
		})
		class SecondServiceModule {}

		@Module({
			imports: [
				FirstServiceModule,
				SecondServiceModule,
			],

			providers: [
				{
					provide: NestedService,
					useValue: {
						value: nestedServiceValue,
					},
				},
			],
		})
		class ParentModule {}

		@Module({
			imports: [
				ParentModule,
			],
		})
		class ApplicationModule {}

		application = await createTestApplication(ApplicationModule);

		const firstService = application.get(FirstService);
		const secondService = application.get(SecondService);

		expect(firstService.nestedService).toEqual({
			value: nestedServiceValue,
		});

		expect(secondService.nestedService).toEqual({
			value: nestedServiceValue,
		});

		expect(firstService.nestedService).toBe(secondService.nestedService);
	});
});
